# 9. Hybrid App Type

Date: 2021-04-06

## Status

Accepted

## Context

The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

Server-base의 하이브리드 앱으로 구축

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
