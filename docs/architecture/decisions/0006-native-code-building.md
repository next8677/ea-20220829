# 6. Native Code Building

Date: 2021-04-02

## Status

Proposed

## Context

1. 대부분 UI는 WebView로 가져가며, Native는 최소화한다.
2. SPA 웹화면 개발과 Native 코드를 별도로 할 시, 연동시 예상치 못한 문제가 발생할 수 있어 동일한 code base로 갈 필요 있다.
3. Android는 Android Studio, iOS는 XCode로 다른 개발환경이다.
4. Native First vs SPA First 비교
    - Native scratch code나 CNS 기 보유 코드에서 시작하여 WebView에 SPA를 연결하고, WebView 연동 모듈을 개발하는 방식으로 할 것인가?
    - SPA code에서 시작하여, Native Code는 SPA가 못하는 부분만 PlugIn으로 개발하여 붙이는 방식으로 할 것인가?
5. Native First 개발 방식 비교 (Scratch vs MyData 코드)
    - From scratch : 기술부채없이 깨끗한 코드로 시작할 수 있으나, 개발 기간 오래 걸림
    - From MyData Code : 개발 기간을 줄일 수 있으나, 기술부채를 안고 가야하며, 협력사에게 익숙치 않을 수 있음
6. SPA First 개발 방식 비교
    - Cordova : WebView를 기본으로 Android, iOS 공통 코드를 빌드하며, Native code는 PlugIn으로 연결한다.
    - Capacitor : Cordova를 기본으로 Ionic에서 개선하여 오픈소스화한 것으로 기능상 Cordova와 동일 
    - Ionic : Corodova나 Capacitor 의 WebView framework에 UI element들을 통합해 놓은 통합 개발 환경
7. Cordova가 Apache 재단의 프로젝트로 가장 넓은 개발자풀을 가지고 있고, Android/iOS 코드를 동일 구조로 가져 갈 수 있이 Cordova 기반으로 개발하는 것이 최선이라 판단됨. 

## Decision

Cordova CLI를 Android, iOS 공통으로 사용하여, 동일 source repository를 사용한다.

## Consequences

1. Cordova + iOS/Android 기본 코드 구조를 만든다.
2. Web <-> Native Interface 정의서를 jsdoc으로 뽑아낼 수 있게 한다.
3. CI 서버는 iOS/Android 둘다 빌드 가능한 Mac Pro가 가능한지 확인한다.