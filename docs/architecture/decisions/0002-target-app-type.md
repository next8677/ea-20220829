# 2. target app type

Date: 2021-03-26

## Status

Accepted

## Context

- K의 RFP에 사용자 경험이 중요한 화면에 대해 생동감 넘치게 Native로 개발 고려와 새로운 기술의 SPA framework을 도입 요청 요청
- 3/26 K와 컨퍼런스 call에 질문했을 때 WebView로 충분히 Native와 비슷한 화면이 나오면 되고, Native로 꼭 만들어야만 화면이 있는 것이 아니다라는 답변
- Native 화면 개발은 개발 공수가 많이 들어 8월 완료 불가능

## Decision

외부 솔루션 연계 부분은 Native로 개발하고, 화면을 WebView로 개발한다

## Consequences

Benefits

- 개발 생산성 향상
- Android와 iOS 에서 UI 부분은 동일 소스 공유

Risks

- SPA 개발에 필요한 디자이너와 개발자 소싱
